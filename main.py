import os
import sys
import random
from datetime import datetime

import cv2
import deprecated
import numpy as np
import sklearn.model_selection

import DetectronExperiment
from DataAnnotation import load_dom_csv, show_groundtruth_segments, get_segment_dicts, merge_runner
from DetectronExperiment import train_model, evaluate_model
from ImageComparer import imagehash_approach
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.checkpoint import DetectionCheckpointer
from detectron2.utils.visualizer import Visualizer
from detectron2.utils.visualizer import ColorMode
from detectron2.data.datasets.coco import convert_to_coco_dict
from detectron2.data import build_detection_test_loader
from contextlib import ExitStack, contextmanager
from torch import nn

import torch, torchvision
import argparse
import logging

from Screenshotter import create_screenshot


@contextmanager
def inference_context(model):
    """
    A context where the model is temporarily changed to eval mode,
    and restored to previous mode afterwards.
    Args:
        model: a torch Module
    """
    training_mode = model.training
    model.eval()
    yield
    model.train(training_mode)


def main(filepath, csvpath, output_dir, save_img_segments=False, train_pct=0.8, random_state=5, continue_training=False,
         config_path="/data/experiments/goldmann/config.yaml", image_dest=None, remove_small=False,
         use_oversampling=False, remove_small_threshold=400, single_evaluation=False, experiment_name="",
         use_reduced=False, use_merged=False, label_binary=False, complex_labeling=False, complex_labels=None,
         local=False):
    if not image_dest:
        image_dest = output_dir + experiment_name + "/images/"
        print("Image dest: {}".format(image_dest))
    if save_img_segments:
        imagehash_approach(filepath)

    # From Dataset create train and test 4:1
    # Load information correspondingly
    data_idx = dict()
    if use_reduced:
        data_idx_list = sorted(os.listdir(filepath))[:200]
    else:
        data_idx_list = sorted(os.listdir(filepath))

    if use_merged:
        merge_runner(filepath)
    data_idx["train"], data_idx["val"] = sklearn.model_selection.train_test_split(data_idx_list, train_size=train_pct,
                                                                                  random_state=random_state)
    csv_dom_nodes = load_dom_csv(csvpath, data_idx_list)

    for algo_step in ["train", "val"]:
        DatasetCatalog.register("segmentation_" + algo_step,
                                lambda d=algo_step: get_segment_dicts(filepath, dom_dict=csv_dom_nodes,
                                                                      file_idx_to_load=data_idx[d],
                                                                      remove_small_samples=remove_small,
                                                                      oversampling=use_oversampling,
                                                                      threshold=remove_small_threshold,
                                                                      use_merged=use_merged,
                                                                      label_binary=label_binary,
                                                                      complex_labeling=complex_labeling,
                                                                      complex_labels=complex_labels))
        if label_binary:
            MetadataCatalog.get("segmentation_" + algo_step).set(
                thing_classes=["Segment"])
        elif complex_labeling:
            MetadataCatalog.get("segmentation_" + algo_step).set(thing_classes=complex_labels)
        else:
            MetadataCatalog.get("segmentation_" + algo_step).set(
                thing_classes=["Left", "Top", "Right", "Bottom", "Unclassified"])

    segmentation_meta_data = MetadataCatalog.get("segmentation_train")

    os.makedirs(output_dir + experiment_name, exist_ok=True)

    model, trainer, cfg = DetectronExperiment.train_model(continue_training,
                                                          config_path=config_path,
                                                          output_directory=output_dir + experiment_name,
                                                          local=local)
    if single_evaluation:
        evaluate_model(trainer, "segmentation_val", cfg, image_idx_single=data_idx['val'])
    else:
        evaluate_model(trainer, "segmentation_val", cfg)

    if label_binary and os.path.exists(cfg.OUTPUT_DIR + "/segmentation_val_coco_format.json"):
        os.remove(cfg.OUTPUT_DIR + "/segmentation_val_coco_format.json.lock")
        os.remove(cfg.OUTPUT_DIR + "/segmentation_val_coco_format.json")

    print("Writing images to disk")
    random.seed(random_state)
    train_loader = build_detection_test_loader(cfg, "segmentation_val")

    # I would like to:
    # See if AP of model is correlated to amount of segments in ground truth
    # AND I would like to visualize the correlation between score, AP, and sample amount

    os.makedirs(image_dest, exist_ok=True)

    with ExitStack() as stack:
        if isinstance(trainer.model, nn.Module):
            stack.enter_context(inference_context(trainer.model))
        stack.enter_context(torch.no_grad())
        gt_idx_list = []
        for count, data in enumerate(train_loader):
            if count >= 10:
                break
            gt_idx_list.append(data[0]['image_id'])
            im = cv2.imread(data[0]["file_name"])

            outputs = model(
                im)  # format is documented at https://detectron2.readthedocs.io/tutorials/models.html#model-output-format
            v = Visualizer(im[:, :, ::-1],
                           metadata=segmentation_meta_data,
                           scale=0.5,
                           instance_mode=ColorMode.IMAGE_BW
                           # remove the colors of unsegmented pixels. This option is only available for segmentation models
                           )
            out = v.draw_instance_predictions(outputs["instances"].to("cpu"))

            cv2.imwrite(image_dest + "original_" + data[0]["image_id"] + ".png", im)

            cv2.imwrite(image_dest + "model_seg_" + data[0]["image_id"] + ".png",
                        out.get_image()[:, :, ::-1])
        show_groundtruth_segments(gt_idx_list, image_dest, filepath=filepath, csv_dom_nodes=csv_dom_nodes,
                                  remove_small_samples=remove_small, threshold=remove_small_threshold,
                                  use_merged=use_merged, label_binary=label_binary, complex_labeling=complex_labeling,
                                  complex_labels=complex_labels)


def arg_parser():
    parser = argparse.ArgumentParser(description='Choose run configurations!')
    parser.add_argument('-N', metavar='experiment_name', type=str,
                        help='Name for the suffix to use for experiments as String', default="")
    parser.add_argument('-D', metavar='data_path', type=str, help='Absolute path to the directory as String',
                        default="/data/experiments/goldmann/webis-webseg-20")
    parser.add_argument('-C', metavar='csv_path', type=str,
                        help='Absolute path to the directory with csv information as String',
                        default="/data/experiments/goldmann/webis-webseg-20")
    parser.add_argument('-L', metavar='log_path', type=str, help='Absolute path to the log directory',
                        default="/data/experiments/goldmann/output/")
    parser.add_argument('-O', metavar='output_dir', type=str, help='Absolute path to the desired output directory',
                        default="/data/experiments/goldmann/output_")
    parser.add_argument('-c', metavar='config_path', type=str,
                        help='Relative path to the config.yaml to be used as String',
                        default="/data/experiments/goldmann/config.yaml")
    parser.add_argument('-r', action='store_true', help='Set flag to resume previous training from last checkpoint',
                        default=False)
    parser.add_argument('-o', action='store_true', help='Set flag to intentionally oversample small values',
                        default=False)
    parser.add_argument('-s', action='store_true', help='Set flag to remove small images from the dataset be removed',
                        default=False)
    parser.add_argument('-s_val', metavar='small_threshold', type=int,
                        help='Value to be used for what is a small image, ignore if -s not set', default=400)
    parser.add_argument('-e', action='store_true', help='Set flag to evaluate each image singularly',
                        default=False)
    parser.add_argument('-dummy', action='store_true', help='Set flag to evaluate on small sample of 200',
                        default=False)
    parser.add_argument('-m', action='store_true', help='Set flag to use groundtruth - merged instead of normal '
                                                        'groundtruth', default=False)
    parser.add_argument('-b', action='store_true', help="Use only binary labels i.e. segment and non segment",
                        default=False)
    parser.add_argument('-cl', action='store_true', help="Use complex labeling scheme", default=False)
    parser.add_argument('-d', action='store_true', help="Enable live demo mode", default=False)
    return parser.parse_args()


def log_args(arg_config):
    logging.info("Experiment name: " + arg_config.N)
    logging.info("Dataset directory is set to: " + arg_config.D)
    logging.info("CSV directory is set to: " + arg_config.C)
    logging.info("Path to configuration used is set to: " + arg_config.c)
    logging.info("Resume training is set to: " + str(arg_config.r))
    logging.info("Oversampling is set to: " + str(arg_config.o))
    logging.info("Removal of small samples is set to: " + str(arg_config.s))
    logging.info("Removal of small samples threshold is set to: " + str(arg_config.s_val))
    logging.info("Evaluation of each image is set to: " + str(arg_config.e))
    logging.info("Usage of reduced data is set to: " + str(arg_config.dummy))
    logging.info("Usage of merged ground truth is set to: " + str(arg_config.m))
    logging.info("Using only binary labels is set to: " + str(arg_config.b))
    logging.info("Using complex labeling is set to: " + str(arg_config.cl))


def segment_example_image(link_to_page, config_path):
    filename = create_screenshot(link_to_page)
    if not filename:
        return
    model = DetectronExperiment.get_predictor(config_path=config_path)
    MetadataCatalog.get("segmentation_test").set(
        thing_classes=["Left", "Top", "Right", "Bottom", "Unclassified"])
    segmentation_meta_data = MetadataCatalog.get("segmentation_test")

    with torch.no_grad():
        print("Loading and segmenting image")
        im = cv2.imread(filename)
        outputs = model(im)  # format is documented at https://detectron2.readthedocs.io/tutorials/models.html#model-output-format
        v = Visualizer(im[:, :, ::-1],
                       metadata=segmentation_meta_data,
                       scale=0.5,
                       instance_mode=ColorMode.IMAGE_BW
                       # remove the colors of unsegmented pixels. This option is only available for segmentation models
                       )
        out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
        cv2.imwrite("generated_segmentation.png", out.get_image()[:, :, ::-1])
    print("Finished image segmentation")

if __name__ == '__main__':
    args = arg_parser()
    logging.basicConfig(filename=args.L + str(datetime.today().date()) + '.log', level=logging.INFO)
    log_args(args)
    all_labels = ['social media', 'uncategorized', 'visual break', 'input', 'sidebar',
                  'table', 'outer link', 'nav element', 'header', 'footer', 'content',
                  'list element', 'media', 'inner link', 'text', 'div',
                  'dynamic', 'No DOM']
    if args.d:
        print("Demo Mode has been enabled!")
        print("Please input the url of the webpage or the filepath of the screenshot to be segmented:")
        link = input()
        segment_example_image(link, config_path=args.c)
    else:
        main(args.D, args.C, output_dir=args.O, continue_training=args.r, config_path=args.c, remove_small=args.r,
             use_oversampling=args.o, single_evaluation=args.e, use_reduced=args.d, experiment_name=args.N,
             use_merged=args.m, label_binary=args.b, complex_labeling=args.cl, complex_labels=all_labels, local=False)
