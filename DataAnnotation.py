"""
This file deal with dataset annotation, manipulation etc.
"""
# left, bottom, right, top
import csv
import json
import os
import re
from collections import defaultdict
from xml.etree.ElementTree import Element
from urllib.parse import urlparse

import cv2
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
from detectron2.structures import BoxMode
from detectron2.utils.visualizer import Visualizer
from lxml import etree

from DetectronExperiment import determine_oversample_rate


def generate_bbox(nested_data):
    flat_image_data = []
    for anno in nested_data:
        for inner in anno:
            for poly in inner:
                # both bbox and poly might be numpy array type, make sure this isn't a problem
                # Label here
                bbox = np.concatenate((np.min(poly, axis=0), np.max(poly, axis=0))).tolist()
                flat_image_data.append(bbox)
    return flat_image_data


# Idea is simple:
# For each segment of each image, if it's area is below a certain threshold x merge it with neighbours which at most are
# y units away


def can_merge(bx_a, bx_b, threshold):
    x_dist = min(abs(bx_a[0] - bx_b[0]), abs(bx_a[0] - bx_b[2]), abs(bx_a[2] - bx_b[0]), abs(bx_a[2] - bx_b[2]))
    y_dist = min(abs(bx_a[1] - bx_b[1]), abs(bx_a[1] - bx_b[3]), abs(bx_a[3] - bx_b[1]), abs(bx_a[3] - bx_b[3]))
    if x_dist < threshold or y_dist < threshold:
        return True
    else:
        return False


def format_gt(new_data, data_dict, dataset_path):
    segs = []
    for bbox in new_data:
        poly = [[min(bbox[0], bbox[2]), max(bbox[1], bbox[3])], [max(bbox[0], bbox[2]), max(bbox[1], bbox[3])],
                [max(bbox[0], bbox[2]), min(bbox[1], bbox[3])], [min(bbox[0], bbox[2]), min(bbox[1], bbox[3])]]
        poly.append(poly[0])
        segs.append([[poly]])
    maj_vote = {"majority-vote": segs}
    data_dict['segmentations'] = maj_vote
    with open(os.path.join(dataset_path, data_dict['id'], "ground-truth-merged.json"), "w+") as f:
        json.dump(data_dict, f)
    return


def merge_small_connected_areas(area_increase_tolerance, image_data, data_path):
    # format is min(x,y) max(x,y)
    # Basically we go over all boxes:
    # For each box we check if it can be merged with any other box
    # If yes merge them and remove both from bboxes and add the new merged box
    # If a box can't be merged we keep it in the dataset
    bboxes = generate_bbox(image_data['segmentations']['majority-vote'])
    new_data = []
    while bboxes:
        to_merge = bboxes.pop(0)
        have_merged = False
        for one_box in bboxes:
            new_bbox = [min(to_merge[0], one_box[0]), min(to_merge[1], one_box[1]), max(to_merge[2], one_box[2]),
                        max(to_merge[3], one_box[3])]
            new_area = (new_bbox[2] - new_bbox[0]) * (new_bbox[3] - new_bbox[1])
            bbox_one_area = (to_merge[2] - to_merge[0]) * (to_merge[3] - to_merge[1])
            bbox_two_area = (one_box[2] - one_box[0]) * (one_box[3] - one_box[1])
            if new_area <= bbox_one_area + bbox_two_area + area_increase_tolerance:
                bboxes.append(new_bbox)
                bboxes.remove(one_box)
                have_merged = True
                break
        if not have_merged:
            new_data.append(to_merge)
    format_gt(new_data, data_dict=image_data, dataset_path=data_path)
    return image_data


def merge_runner(data_dir):
    for file in os.listdir(data_dir):
        if not os.path.exists(os.path.join(data_dir, file, "ground-truth-merged.json")):
            with open(os.path.join(data_dir, file, "ground-truth.json")) as f:
                data = json.load(f)
            merge_small_connected_areas(image_data=data, area_increase_tolerance=300, data_path=data_dir)
    return


# Returns interactable dom tree that can be queried via xpath from csv


def load_html(html_dir):
    with open(html_dir + "/dom.html", "rb") as html_doc:
        soup = BeautifulSoup(html_doc, 'html.parser')
    dom = etree.HTML(str(soup))
    return dom


def load_dom_csv(csv_dir, file_idx_to_load):
    dom_idx = dict()
    for id_as_string in file_idx_to_load:
        dom = defaultdict(lambda: "DOM doesn't exist")
        with open(csv_dir + "/" + id_as_string + "/nodes.csv", encoding="utf-8") as csvfile:
            reader = csv.DictReader(csvfile)
            c = 0
            for row in reader:
                # In this order its always lower, lower, higher, higher numerical value
                c += 1
                tmp_key = ",".join([row["left"], row["top"], row["right"], row["bottom"]])
                dom[tmp_key] = row['xpath']
        dom_idx[id_as_string] = dom
    return dom_idx


# This method applies labels to segments following a simple labeling scheme taken from
# Efficient Web Browsing on Small Screens by Hamad Ahmadi and Jun Kong
# https://dl.acm.org/doi/pdf/10.1145/1385569.1385576

# Input are the meta information of the image, i.e. image height and width, cutoff scores for segments
# and the polygon


def label_segment_simple(img_height, img_width, bbox, dom_dict):
    poly_string = ",".join([str(i) for i in bbox])
    # order of bbox is left, top, right, bottom
    if bbox[3] < 200:  # top
        return "Top"
    elif bbox[2] < 0.3 * img_width:  # left
        return "Left"
    elif bbox[0] > 0.7 * img_width:  # right
        return "Right"
    elif bbox[1] > img_height - 200:  # bottom
        return "Bottom"
    else:
        return "Unclassified"


def show_groundtruth_segments(data_idx_list, dest_path, csv_dom_nodes=None, filepath="../Dataset/webis-webseg-20",
                              remove_small_samples=False, threshold=400, use_merged=False, label_binary=False,
                              complex_labeling=False, complex_labels=None, use_text_labels=True):
    if not csv_dom_nodes:
        csv_dom_nodes = load_dom_csv(filepath + "-dom-and-nodes/webis-webseg-20", data_idx_list)
    dataset_dicts = get_segment_dicts(filepath, dom_dict=csv_dom_nodes, file_idx_to_load=data_idx_list,
                                      remove_small_samples=remove_small_samples, threshold=threshold,
                                      use_merged=use_merged, label_binary=label_binary,
                                      complex_labeling=complex_labeling,
                                      complex_labels=complex_labels, use_text_labels=use_text_labels)

    for d in dataset_dicts:
        img = cv2.imread(dest_path + "original_" + d["image_id"] + ".png")
        visualizer = Visualizer(img[:, :, ::-1], scale=0.5)
        out = visualizer.draw_dataset_dict(d)
        cv2.imwrite(dest_path + "original_segmentation_" + d["image_id"] + ".png", out.get_image()[:, :, ::-1])


def load_groundtruth():
    data_loc = "../outputs/dummy_test/"
    r = re.compile("^o")
    data_idx_list = [i.split("_")[1].split(".")[0] for i in list(filter(r.match, os.listdir(data_loc)))]
    show_groundtruth_segments(data_idx_list, dest_path=data_loc)


def record_builder(data_dir, id_as_string, use_merged=False):
    if use_merged:
        json_file = os.path.join(data_dir, id_as_string, "ground-truth-merged.json")
    else:
        json_file = os.path.join(data_dir, id_as_string, "ground-truth.json")

    with open(json_file) as f:
        imgs_anns = json.load(f)

    record = {}

    filename = os.path.join(data_dir, imgs_anns["id"])

    record["file_name"] = filename + "/screenshot.png"
    record["image_id"] = imgs_anns["id"]
    record["height"] = imgs_anns["height"]
    record["width"] = imgs_anns["width"]
    annos = imgs_anns["segmentations"]["majority-vote"]
    return record, annos


def add_group_a_labels(dom_element: Element, labels: list, max_gens=2, nav_elem_threshold=3):
    tmp = dom_element
    for gen in range(max_gens):
        if 'id' in tmp.attrib:
            group_a_labels = ["header", "footer", "sidebar", "content"]
            for group_a_label in group_a_labels:
                if group_a_label in tmp.attrib['id']:
                    labels.append(group_a_label)
        tmp = tmp.getparent()
    link_counter = 0
    for child in list(dom_element):
        if child.tag in ['a'] and 'href' in child.attrib:
            link_counter += 1
        if 'id' in child.attrib:
            group_a_labels = ["header", "footer", "sidebar", "content"]
            for group_a_label in group_a_labels:
                if group_a_label in child.attrib['id']:
                    labels.append(group_a_label)
    if link_counter >= nav_elem_threshold or dom_element.tag in ['nav', 'menu', 'course-category-menu', 'paper-tab',
                                                                 'nav-bar']:
        labels.append("nav element")
    return labels


def add_group_b_labels(dom_element: Element, labels: list, domain_dict: defaultdict):
    social_media = ['twitter', 'facebook', 'whatsapp', 'instagram']
    yin_ad_tags = ["advertisement", "classified", "sponsor", "sale"]
    domain = None
    if dom_element.tag in ['a']:
        if 'href' in dom_element.attrib:
            labels.append('link')
            domain = urlparse(dom_element.attrib['href']).netloc
            domain_dict[domain] += 1
        else:
            labels.append('text')
        # Collect domain, heuristic: most frequent is our domain rest is ad
    elif dom_element.tag in ['twitterwidget'] or (
            'class' in dom_element.attrib and any([med in dom_element.attrib['class'] for med in social_media])):
        labels.append('social media')
    elif 'class' in dom_element.attrib and any([ad in dom_element.attrib['class'] for ad in yin_ad_tags]):
        labels.append("outer link")
        print("Found an ad!")
    return labels, domain_dict, domain


def add_group_c_labels(dom_element: Element, labels: list, depth_remaining: int):
    if depth_remaining == 0:
        return labels
    depth_remaining -= 1
    media_tags = ['img', 'iframe', 'svg', 'figure', 'yt-formatted-string', 'ytd-topbar-logo-renderer', 'embed',
                  'canvas', 'g', 'object', 'foreignobject', 'ytd-playability-error-supported-renderers', 'rect', 'use',
                  'video', 'path', 'ellipse', 'circle', 'figcaption', 'ymaps', 'audio']
    if dom_element.tag in media_tags:
        labels.append('media')
    # if dom_element.tag in ['span', 'p', 'i', 'font']:
    #    labels.append('text')
    for child in list(dom_element):
        # Possibly need to look at parents etc.
        if child.tag in media_tags:
            labels.append('media')
        #    if child.tag in ['span', 'p', 'i', 'font']:
        #        labels.append('text')
        labels = add_group_c_labels(child, labels, depth_remaining)
    return labels


def relabel_links(_link_list: list, domain_dict: defaultdict, inner_bool_arr: list, outer_bool_arr: list):
    main_domain = max(domain_dict, key=domain_dict.get) if len(domain_dict) >= 1 else "No Link"
    for link in _link_list:
        if link == main_domain:
            inner_bool_arr.append(True)
            outer_bool_arr.append(False)
        elif link is None:  # Not link
            inner_bool_arr.append(False)
            outer_bool_arr.append(False)
        else:  # Must be outer link
            inner_bool_arr.append(False)
            outer_bool_arr.append(True)
    return inner_bool_arr, outer_bool_arr


def add_group_d_labels(dom_element, labels):
    # Compare pre and post recursion
    if dom_element.tag in ['span', 'p', 'i', 'font', 'b', 'label', 'pre', 'time', 'strong', 'small', 'blockquote', 'em',
                           'footnotes', 'cite', 'address', 'text', 'legend', 'sup', 'u', 'ins', 'big', 'right',
                           'acronym',
                           'dfn', 'metrics', 'var']:
        labels.append('text')
    elif dom_element.tag in ['td', 'tr', 'tbody', 'table', 'th', 'row', 'thead']:
        labels.append("table")
    elif dom_element.tag in ['li', 'ul', 'dd', 'ol', 'dl', 'dt']:  # Seems to not exist
        labels.append('list element')
    elif dom_element.tag in ['aside']:
        labels.append("sidebar")
    elif dom_element.tag in ['div']:
        labels.append('div')
    elif dom_element.tag in ["h" + str(i) for i in range(1, 7)] + ["hgroup"]:
        labels.append("text")
    elif 'header' in dom_element.tag:
        labels.append('header')
    elif dom_element.tag in ['article', 'section', 'center', 'main', 'body', 'pub-section', 'md-card',
                             'md-card-content']:
        labels.append("content")
    elif dom_element.tag in ['form', 'input', 'select', 'fieldset', 'ytd-searchbox', 'ss-search-bar', 'textarea']:
        labels.append('input')
    elif dom_element.tag in ['button', 'report-abuse']:
        labels.append('input')  # maybe
    elif 'footer' in dom_element.tag:
        labels.append('footer')
    elif dom_element.tag in ['hr']:
        labels.append("visual break")
    return labels


def label_custom_complex(dom_element, domain_dict: defaultdict):
    # Labels are: Footer, Header, Sidebar, Navigational Element, Social Media, Media(Video + Image), Advertisement
    _labels = []
    # Group A: Footer, Header, Sidebar, Main Content (Tag based)
    _labels = add_group_a_labels(dom_element, _labels)
    # Group B: Social Media, Advertisement, Navigation Element (Link based, hrefs, a)
    _labels, domain_dict, link = add_group_b_labels(dom_element, _labels, domain_dict=domain_dict)
    # Group C: Media, Textblock (Content based)
    _labels = add_group_c_labels(dom_element, _labels, depth_remaining=2)
    # Group D: Any other tag: Table, Label, Text,
    _labels = add_group_d_labels(dom_element, _labels)
    # Group E: Non DOM linked segments, Uncategorized
    if len(_labels) == 0:
        _labels.append("uncategorized")
        print(dom_element)
    # TODO: What happens if a segment fulfills multiple criteria? Probability function? Multiclass?
    # Analyze neighborhood? Classify dom nodes?
    return _labels, domain_dict, link


def get_segment_dicts(data_dir, dom_dict, file_idx_to_load, remove_small_samples=False, threshold=400,
                      oversampling=False, use_merged=False, label_binary=False, complex_labeling=False,
                      complex_labels=None, use_text_labels=False):
    dataset_dicts = []
    if complex_labeling:
        complex_dict = json.load(open("/data/experiments/goldmann/complex_labels.json"))
        label_dict = {label: idx for idx, label in enumerate(complex_labels)}
    else:
        label_dict = {"Left": 0, "Top": 1, "Right": 2, "Bottom": 3, "Unclassified": 4, "Segment": 0}

    for id_as_string in file_idx_to_load:
        record, annos = record_builder(data_dir, id_as_string, use_merged=use_merged)
        # Segment is only used for binary testing
        objs = []
        record_counter = 0
        for anno in annos:
            for inner in anno:
                for poly in inner:
                    # both bbox and poly might be numpy array type, make sure this isn't a problem
                    # Label here
                    bbox = np.concatenate((np.min(poly, axis=0), np.max(poly, axis=0)))
                    if label_binary:
                        label = "Segment"
                    elif complex_labeling:
                        label = complex_dict[record['image_id']][record_counter]
                    else:
                        label = label_segment_simple(record["height"], record["width"], bbox,
                                                     dom_dict[record["image_id"]])
                    if remove_small_samples and (bbox[2] - bbox[0]) * (bbox[3] - bbox[1]) < threshold:
                        continue
                    obj = {
                        "bbox": bbox.tolist(),
                        "segmentation": [np.array(poly).flatten().tolist()],
                        # maybe needs to be flattened? np.array(poly).flatten()
                        "bbox_mode": BoxMode.XYXY_ABS,
                        "category_id": label_dict[label] if not use_text_labels else label,
                    }
                    objs.append(obj)
                    record_counter += 1
        record["annotations"] = objs
        if oversampling:
            for i in range(determine_oversample_rate(objs)):
                dataset_dicts.append(record)
        dataset_dicts.append(record)
    return dataset_dicts


def map_html_node_to_segment(csv_dir, data_dir, file_idx_to_load):
    dom_dict = load_dom_csv(csv_dir, file_idx_to_load)
    dataset_dicts = []
    for id_as_string in file_idx_to_load:
        record, annos = record_builder(data_dir, id_as_string)
        seg_to_dom = {}
        seg_id = 0
        for anno in annos:
            for inner in anno:
                for poly in inner:
                    bbox = np.concatenate((np.min(poly, axis=0), np.max(poly, axis=0)))
                    # Can we match this bbox to something in dom_dict
                    seg_id += 1
                    poly_string = ",".join([str(i) for i in bbox])
                    # Might be stupid to use seg id
                    seg_to_dom[seg_id] = dom_dict[record['image_id']][poly_string]

        record["seg_to_dom"] = seg_to_dom
        dataset_dicts.append(record)
    return dataset_dicts


# Those unmapped get label "unmapped", approx 80% or so seem to be mapable


def get_all_parents(html_doc, param):
    split_xpath = param.split("/")
    for i in range(1, len(split_xpath) - 1):
        x = ("/".join(split_xpath[:(-1) * i]))
        # print(html_doc.xpath(x))
    return


def labeler(all_labels):
    file_path = "../Dataset/webis-webseg-20"
    csv_path = "../Dataset/webis-webseg-20-dom-and-nodes/webis-webseg-20"

    html_mapping = map_html_node_to_segment(csv_path, file_path, os.listdir(file_path))
    # html_mapping = map_html_node_to_segment(csv_path, file_path, ["002140"])

    # for each mapped dom seg label it according to schema
    # TODO Make labels their own once loaded file, i.e. don't need to query csv file etc. everytime
    # TODO Generate stats on how many labels etc.
    list_for_df = []
    inner_links_bool_arr, outer_links_bool_arr = [], []

    hard_coded = ["META", "SCROLL-TO", ":", "="]

    for c, seg_xpath in enumerate(html_mapping):
        html_doc = load_html(csv_path + "/" + seg_xpath['image_id'])
        domain_dict = defaultdict(int)
        counter = 0
        link_list = []
        for seg, xpath_str in seg_xpath['seg_to_dom'].items():
            tmp_dict = dict()
            tmp_dict['Image ID'] = seg_xpath['image_id']
            tmp_dict['Internal Count'] = counter
            counter += 1
            link = None
            if "text" in xpath_str:
                xpath_str = "/".join(xpath_str.split("/")[:-1])
            if xpath_str == "DOM doesn't exist":
                labels = ["No DOM"]
            elif any([hard_c in xpath_str for hard_c in hard_coded]):
                labels = ['uncategorized']
            else:
                x_path_eval = html_doc.xpath(xpath_str.lower())
                if len(x_path_eval) == 0:
                    labels = ['dynamic']
                    get_all_parents(html_doc, xpath_str.lower())
                else:
                    labels, domain_dict, link = label_custom_complex(x_path_eval[0], domain_dict=domain_dict)
            for possible_label in all_labels:
                tmp_dict[possible_label] = True if possible_label in labels else False
            list_for_df.append(tmp_dict)
            link_list.append(link)
        if c % 20 == 0:
            print("Done with {} percent!".format(100 * round(c / len(html_mapping), 2)))
        inner_links_bool_arr, outer_links_bool_arr = relabel_links(link_list, domain_dict, inner_links_bool_arr,
                                                                   outer_links_bool_arr)

    df = pd.DataFrame(list_for_df)
    df['inner link'] = inner_links_bool_arr
    df['outer link'] = outer_links_bool_arr
    df.to_csv("LabelAnalysis.csv")


# Function to modify the Labelanalysis.csv into a single label vector:
def sparsify_labels(path_to_csv, all_labels):
    label_dict = defaultdict(list)

    with open(path_to_csv) as f:
        csv_reader = csv.reader(f)
        next(csv_reader, None)  # skip the headers
        for l in csv_reader:
            try:
                label_idx = l.index("True") - 3
            except ValueError:
                label_idx = -1
            label_dict[l[1]].append(all_labels[label_idx])
    json.dump(label_dict, open("complex_labels.json", "w+"))


def simple_labeling(data_dir, dom_dict, file_idx_to_load):
    label_dict = {}
    for id_as_string in file_idx_to_load:
        record, annos = record_builder(data_dir, id_as_string)
        # Segment is only used for binary testing
        objs = []
        for anno in annos:
            for inner in anno:
                for poly in inner:
                    # both bbox and poly might be numpy array type, make sure this isn't a problem
                    # Label here
                    bbox = np.concatenate((np.min(poly, axis=0), np.max(poly, axis=0)))

                    label = label_segment_simple(record["height"], record["width"], bbox,
                                                     dom_dict[record["image_id"]])
                    objs.append(label)
        label_dict[record["image_id"]] = objs
    json.dump(label_dict, open("simple_labels.json", "w+"))


if __name__ == "__main__":
    all_labels = ['social media', 'uncategorized', 'visual break', 'input', 'sidebar',
                  'table', 'outer link', 'nav element', 'header', 'footer', 'content',
                  'list element', 'media', 'inner link', 'text', 'div',
                  'dynamic', 'No DOM']
    #labeler(all_labels)
    #sparsify_labels("./LabelAnalysis.csv", all_labels)

    #file_path = "../Dataset/webis-webseg-20"
    #csv_path = "../Dataset/webis-webseg-20-dom-and-nodes/webis-webseg-20"
    #data_idx_list = os.listdir(file_path)
    #csv_dom_nodes = load_dom_csv(csv_path, data_idx_list)
    #simple_labeling(file_path, csv_dom_nodes, data_idx_list)

    # data_loc = "/home/fabio/MasterThesis/Dataset/dummy/000000/"
    # merge_runner("../Dataset/webis-webseg-20/000571")
    show_groundtruth_segments(['000045', "000571", "003664", "003711", "006632", "006861", "006994", "007173", "007746", "009270"], dest_path="../experiment_eval/small_ap_large/images/", remove_small_samples=True, threshold=1024)

