This is the associated code to my Master Thesis "Title".
The project is fully dockerized and can be rebuilt using the Dockerfile contained in the repo.

This work directly builds off of the work seen in https://github.com/facebookresearch/detectron2. The full dataset can be accessed via: https://zenodo.org/record/3988124. All the documents within this repository are also located in the TUBCloud under: https://tubcloud.tu-berlin.de/s/jw5HF6AqxcikPc5

All requirements are listed in requirements.txt

config.yaml is the configuration file used for the Detectron2 model.

In DataAnnotation.py all the code regarding the labeling and dataset modification lies.

DetectronExperiment.py regards takes care of the interface to Detectron2 and runs all the necessary steps for training.

Screenshotter.py and ImageComparer.py are used to generate screenshots for the demo mode and to generate the image similarity.

Tesseract.py creates multiple files: It segments all images passed to it and saves the single segments before creating the file seg_to_text.csv which is all the recognized text in the individual segments. Current configuration allows recognition of the five common languages: Arabic, Simple Chinese, German, English and French and can easily be expanded via CLI.

For the demo mode the default model is located in model_final.pth, which can be overwritten. The demo mode prints instructions to the command line and can use either urls or device-based screenshots.

10 selected samples with all screenshots and prediction outputs for all examples, as well as all individual segments of the baseline experiment are contained in the all_images.zip file.

main.py is the main runner of the experiments which can be run with any of the following parameters:


flag| metavar | description | default
----|---------|-------------|--------
-N | experiment_name  |   Name for the suffix to use for experiments as String            | default=""
-D | data_path        |   Absolute path to the data directory as String                   | default="/data/experiments/goldmann/webis-webseg-20
-C | csv_path         |   Absolute path to the directory with csv information as String   |default="/data/experiments/goldmann/webis-webseg-20"
-L | log_path         |   Absolute path to the log directory                              |default="/data/experiments/goldmann/output/"
-O | output_dir       |   Absolute path to the desired output directory                   |default="/data/experiments/goldmann/output_"
-c | config_path      |   Relative path to the config.yaml to be used as String           |default="/data/experiments/goldmann/config.yaml"
-r |                  |   Set flag to resume previous training from last checkpoint       |default=False
-o |                  |   Set flag to intentionally oversample small samples              |default=False
-s |                  |   Set flag to remove small images from the dataset                |default=False
-s_val |              |   Value to be used for small cutoff, ignore if -s not set         |default=400
-e |                  |   Set flag to evaluate each image singularly                      |default=False
-dummy |              |   Set flag to evaluate on small sample set of 200                 |default=False
-m |                  |   Set flag to use ground truth - merged                           |default=False
-b |                  |   Set flag to use binary labels                                   |default=False
-cl |                 |   Set flag to use complex labels                                  |default=False
-d |                  |   Set flag to enable demonstration mode                           |default=False  


To start the program use: "python3 main.py" with any flags as needed within a docker container.
If retraining from previous model model should be put within the root level.


For any questions please contact me under f.v.goldman@gmail.com or f.vonschellinggoldman@campus.tu-berlin.de directly.
