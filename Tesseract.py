import csv
import json
import os
from collections import defaultdict

from PIL import Image
import pytesseract


def open_segments(file_folder):
    languages = "+".join(["eng", "deu", "chi_sim", "fra", "ara"])
    with open(os.path.join(file_folder, "seg_to_text.csv"), "w+") as csvf:
        writer = csv.writer(csvf)
        writer.writerow(["Image ID", "Count", "Text"])
        for file_id in os.listdir(file_folder):
            if file_id == "seg_to_text.csv":
                continue
            for file in os.listdir(os.path.join(file_folder, file_id, "segmented")):
                text = pytesseract.image_to_string(Image.open(os.path.join(file_folder, file_id, "segmented", file)), lang=languages)
                id_count = file.split(".")[0].split("_")
                writer.writerow([id_count[1], id_count[2], text])


# is bbox xyxy or xywh
def generate_segment(experiment_folder, image_id, bbox, destination_folder, seg_counter):
    im = Image.open(os.path.join(experiment_folder, "images", "original_" + image_id + ".png"))
    box = (bbox[0], bbox[1], bbox[0] + bbox[2], bbox[1] + bbox[3])
    cropped_im = im.crop(box)
    with open(os.path.join(destination_folder, image_id, "segmented", "cropped_" + image_id + "_" + str(seg_counter) + ".png"), "bw+") as f:
        cropped_im.save(f)


def segment_all_images(experiment_folder, destination_folder):
    with open(os.path.join(experiment_folder, "coco_instances_results.json")) as f:
        pred_segments = json.load(f)
    seg_counter_dict = defaultdict(int)
    valid_ids = ['000045', '000571', '003664', '003711', '006632', '006861', '006994', '007173', '007746', '009270']
    for segment_data in pred_segments:
        if not segment_data['image_id'] in valid_ids:
            continue
        generate_segment(experiment_folder, segment_data['image_id'], segment_data['bbox'], destination_folder, seg_counter_dict[segment_data['image_id']])
        seg_counter_dict[segment_data['image_id']] += 1


def main(experiment_result_path, destination_loc="../Thesis/images"):
    for experiment_folder in sorted(next(os.walk(experiment_result_path))[1]):
        if experiment_folder != "baseline":
            continue
        segment_all_images(os.path.join(experiment_result_path, experiment_folder), destination_loc)
        open_segments(destination_loc)


if __name__ == "__main__":
    main("../experiment_eval")