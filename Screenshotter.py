from selenium import webdriver
from time import sleep
import validators
import os
from selenium.webdriver.firefox.options import Options


def create_screenshot(link="https://www.dfki.de"):
    if validators.url(link):
        filename = "screenshot.png"
        options = Options()
        options.headless = True
        print("Creating screenshot from webpage")
        driver = webdriver.Firefox(options=options)
        driver.get(link)
        sleep(1)

        driver.get_screenshot_as_file("{}".format(filename))
        driver.quit()
        print("Finished creating the screenshot")
        return filename

    elif os.path.exists(link):
        return link

    else:
        print("Please input either a valid path or url to generate the image.")
        return None


def main():
    create_screenshot()


if __name__ == "__main__":
    main()