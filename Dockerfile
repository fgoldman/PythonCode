# This file is a template, and might need editing before it works on your project.
FROM nvidia/cuda:10.2-base


RUN apt-get update && apt-get install --no-install-recommends --no-install-suggests -y curl
RUN apt-get install unzip
RUN apt-get install git -y
RUN apt-get install vim -y
RUN apt-get update && apt-get install libicu-dev libicu-dev libcairo2-dev -y
RUN apt-get install libtesseract-dev -y
RUN apt-get install -y tesseract-ocr-ara tesseract-ocr-deu tesseract-ocr-fra tesseract-ocr-chi-sim


RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
RUN apt-get -y install python3.6-tk
RUN apt-get -y install python3-pip
RUN pip3 install --upgrade pip

WORKDIR /PythonCode

COPY requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt
RUN pip3 install 'git+https://github.com/facebookresearch/detectron2.git'

COPY . .

RUN apt-get update ##[edited]
RUN apt-get install ffmpeg libsm6 libxext6  -y


# For some other command
#CMD ["python3", "main.py", "detectron"]
#CMD ["python3", "main.py", "dummy"]
#CMD ["python3", "main.py"]
