# https://colab.research.google.com/drive/1zYNprc--ZA-CO7dfK06OZTbhxbllBqZ-#scrollTo=9_FzH13EjseR
# Detectron imports

import os
import numpy as np

# Some basic setup:
# Setup detectron2 logger

# import some common detectron2 utilities
import torch
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.engine import DefaultTrainer
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.data import build_detection_test_loader
from contextlib import ExitStack, contextmanager

from torch import nn
import yaml


@contextmanager
def inference_context(model):
    """
    A context where the model is temporarily changed to eval mode,
    and restored to previous mode afterwards.
    Args:
        model: a torch Module
    """
    training_mode = model.training
    model.eval()
    yield
    model.train(training_mode)


def get_all_inputs_outputs(data_loader, model):
    for data in data_loader:
        yield data, model(data)


def evaluate_model(trainer, dataset_name, cfg, image_idx_single=None):
    print("Beginning Evaluation")
    evaluator = COCOEvaluator(dataset_name, tasks=("bbox", "segm"), distributed=True, output_dir=cfg.OUTPUT_DIR)
    val_loader = build_detection_test_loader(cfg, dataset_name)
    if image_idx_single:
        evaluator.reset()
        with ExitStack() as stack:
            if isinstance(trainer.model, nn.Module):
                stack.enter_context(inference_context(trainer.model))
            stack.enter_context(torch.no_grad())
            for inputs, outputs in get_all_inputs_outputs(model=trainer.model, data_loader=val_loader):
                evaluator.process(inputs, outputs)
        for image_id in image_idx_single:
            eval_results = evaluator.evaluate(img_ids=[image_id])
            print(eval_results)
    else:
        print(inference_on_dataset(trainer.model, val_loader, evaluator))
    print("Finished Evaluation")


def calc_bbox_area(bbox):
    return abs(bbox[2] - bbox[0]) * abs(bbox[3] - bbox[1])


def sample_func(mean, variance, base_rate=150000, var_weight=2):
    # over_sample_factor = (base_rate - mean) + (variance * var_weight)
    # I want a function that if mean is low is likelier to give higher output
    if mean > base_rate:
        return 0
    else:
        return 3
    #return max(0, over_sample_factor)


def determine_oversample_rate(image_data):
    areas = [calc_bbox_area(d['bbox']) for d in image_data]
    m = np.mean(areas)
    v = np.var(areas)

    # return fct, s.t. if var is high => oversample more + base rate depending on size, i.e. the smaller avg the more we over sample
    return sample_func(m, v)


def get_predictor(config_path="/data/experiments/goldmann/config.yaml"):
    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    cfg.merge_from_file(config_path)
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # Set threshold for this model
    cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")
    predictor = DefaultPredictor(cfg)
    return predictor


def train_model(continue_training=False, config_path="/data/experiments/goldmann/config.yaml",
                output_directory="/data/experiments/goldmann/output", local=False):
    cfg = get_cfg()

    cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    cfg.merge_from_file(config_path)
    cfg.DATASETS.TEST = ()
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")  # Let training initialize from model zoo
    cfg.OUTPUT_DIR = output_directory

    os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)

    trainer = DefaultTrainer(cfg)
    trainer.resume_or_load(resume=continue_training)
    if not local:
        trainer.train()
    cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")  # path to the model we just trained
    predictor = DefaultPredictor(cfg)
    trainer = DefaultTrainer(cfg)
    trainer.resume_or_load(resume=continue_training)
    return predictor, trainer, cfg




